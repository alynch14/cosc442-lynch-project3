package edu.towson.cis.cosc442.project3.vendingmachine.manualtests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VendingMachineExceptionTest {
	private VendingMachineException myException;

	@Before
	public void setUp() throws Exception {
		myException = new VendingMachineException();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertNotNull(myException);
	}

}
